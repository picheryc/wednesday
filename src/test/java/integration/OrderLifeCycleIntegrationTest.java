package integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.JsonPath;
import domain.order.DishInOrder;
import domain.order.TableInOrder;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest(classes = {application.Application.class})
@AutoConfigureMockMvc
class OrderLifeCycleIntegrationTest {
    @Autowired
    private MockMvc mvc;

    @Test
    @DisplayName("Permet de créer une commande")
    void testCanCreateOrder() throws Exception {
        ResultActions result = mvc.perform(MockMvcRequestBuilders.post("/orders").accept(MediaType.APPLICATION_JSON));
        result.andExpect(status().isOk())
                .andExpect(jsonPath("$").exists());
    }


    @Test
    @DisplayName("Permet de visualiser le contenu d'une commande")
    void testCanQueryOrder() throws Exception {
        ResultActions createOrderResult = mvc.perform(MockMvcRequestBuilders.post("/orders").accept(MediaType.APPLICATION_JSON));
        String orderId = JsonPath.read(createOrderResult.andReturn().getResponse().getContentAsString(), "$");

        ResultActions queryOrderResult = mvc.perform(MockMvcRequestBuilders.get("/orders/" + orderId).accept(MediaType.APPLICATION_JSON));
        String actualOrderId = JsonPath.read(queryOrderResult.andReturn().getResponse().getContentAsString(), "$.orderId");

        assertEquals(orderId, actualOrderId);
    }

    @Test
    @DisplayName("Permet de récupérer toutes les commandes")
    void testCanQueryAllOrders() throws Exception {
        ResultActions createOrderResult = mvc.perform(MockMvcRequestBuilders.post("/orders").accept(MediaType.APPLICATION_JSON));
        String orderId = JsonPath.read(createOrderResult.andReturn().getResponse().getContentAsString(), "$");

        ResultActions queryOrdersResult = mvc.perform(MockMvcRequestBuilders.get("/orders").accept(MediaType.APPLICATION_JSON));

        List<LinkedHashMap> orders = JsonPath.read(queryOrdersResult.andReturn().getResponse().getContentAsString(), "$");
        List<String> orderIds = new ArrayList(orders.stream().map(order -> order.get("orderId")).collect(Collectors.toList()));
        assertTrue(orderIds.contains(orderId));
    }

    @Test
    @DisplayName("Doit associer un timestamp correspondant à la date de creation")
    void testCreateOrderWithTimestamp() throws Exception {
        ResultActions createOrderResult = mvc.perform(MockMvcRequestBuilders.post("/orders").accept(MediaType.APPLICATION_JSON));
        String orderId = JsonPath.read(createOrderResult.andReturn().getResponse().getContentAsString(), "$");
        ResultActions queryOrderResult = mvc.perform(MockMvcRequestBuilders.get("/orders/" + orderId).accept(MediaType.APPLICATION_JSON));
        String createdAt = JsonPath.read(queryOrderResult.andReturn().getResponse().getContentAsString(), "$.createdAt");

        assertNotNull(createdAt);
    }

    @Test
    @DisplayName("Permet de supprimer une commande")
    void testCanDeleteOrder() throws Exception {
        ResultActions createOrderResult = mvc.perform(MockMvcRequestBuilders.post("/orders").accept(MediaType.APPLICATION_JSON));
        String orderId = JsonPath.read(createOrderResult.andReturn().getResponse().getContentAsString(), "$");

        mvc.perform(MockMvcRequestBuilders.delete("/orders/" + orderId).accept(MediaType.APPLICATION_JSON));

        ResultActions queryOrdersResult = mvc.perform(MockMvcRequestBuilders.get("/orders").accept(MediaType.APPLICATION_JSON));
        List<LinkedHashMap> orders = JsonPath.read(queryOrdersResult.andReturn().getResponse().getContentAsString(), "$");
        List<String> orderIds = new ArrayList(orders.stream().map(order -> order.get("orderId")).collect(Collectors.toList()));
        assertFalse(orderIds.contains(orderId));
    }

    @Test
    @DisplayName("Permet d'ajouter un plat dans une commande existante")
    void testAddADishInExistingOrder() throws Exception {
        ResultActions createOrderResult = mvc.perform(MockMvcRequestBuilders.post("/orders").accept(MediaType.APPLICATION_JSON));
        String orderId = JsonPath.read(createOrderResult.andReturn().getResponse().getContentAsString(), "$");

        String expectedId = "vegan_nem";
        DishInOrder dish = new DishInOrder(expectedId);
        ObjectMapper objectMapper = new ObjectMapper();
        String json = objectMapper.writeValueAsString(dish);

        ResultActions addDishResult = mvc.perform(MockMvcRequestBuilders.post(
                MessageFormat.format("/orders/{0}/dishes", orderId))
                .content(json)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON));
        addDishResult.andExpect(status().isOk());

        ResultActions getOrderResult = mvc.perform(MockMvcRequestBuilders.get(MessageFormat.format("/orders/{0}", orderId)).accept(MediaType.APPLICATION_JSON));
        getOrderResult.andExpect(status().isOk());
        List<LinkedHashMap> dishes = JsonPath.read(getOrderResult.andReturn().getResponse().getContentAsString(), "$.dishes");
        assertEquals(1, dishes.size());
        assertEquals(expectedId, dishes.get(0).get("id"));
    }

    @Test
    @DisplayName("Permet d'ajouter une table dans une commande existante")
    void testAddATableInExistingOrder() throws Exception {
        ResultActions createOrderResult = mvc.perform(MockMvcRequestBuilders.post("/orders").accept(MediaType.APPLICATION_JSON));
        String orderId = JsonPath.read(createOrderResult.andReturn().getResponse().getContentAsString(), "$");
        ResultActions getAllTablesResult = mvc.perform(MockMvcRequestBuilders.get("/restaurant/tables").accept(MediaType.APPLICATION_JSON));
        List<LinkedHashMap> tables = JsonPath.read(getAllTablesResult.andReturn().getResponse().getContentAsString(), "$");
        List<String> tableIds = new ArrayList(tables.stream().map(table -> table.get("id")).collect(Collectors.toList()));
        String tableId = tableIds.get(0);

        TableInOrder table = new TableInOrder(tableId);
        ObjectMapper objectMapper = new ObjectMapper();
        String json = objectMapper.writeValueAsString(table);

        ResultActions addTableResult = mvc.perform(MockMvcRequestBuilders.post(
                MessageFormat.format("/orders/{0}/table", orderId))
                .content(json)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON));
        addTableResult.andExpect(status().isOk());

        ResultActions getOrderResult = mvc.perform(MockMvcRequestBuilders.get(MessageFormat.format("/orders/{0}", orderId)).accept(MediaType.APPLICATION_JSON));
        getOrderResult.andExpect(status().isOk());
        LinkedHashMap actualTable = JsonPath.read(getOrderResult.andReturn().getResponse().getContentAsString(), "$.table");
        assertEquals(tableId, actualTable.get("id"));
    }

    @Test
    @DisplayName("Permet de récupérer le montant d'une commande avec un seul plat")
    void testReturnsTotalAmountForOneDishOrder() throws Exception {
        ResultActions createOrderResult = mvc.perform(MockMvcRequestBuilders.post("/orders").accept(MediaType.APPLICATION_JSON));
        String orderId = JsonPath.read(createOrderResult.andReturn().getResponse().getContentAsString(), "$");

        String expectedId = "vegan_nem";
        DishInOrder dish = new DishInOrder(expectedId);
        ObjectMapper objectMapper = new ObjectMapper();
        String json = objectMapper.writeValueAsString(dish);

        ResultActions addDishResult = mvc.perform(MockMvcRequestBuilders.post(
                MessageFormat.format("/orders/{0}/dishes", orderId))
                                                                        .content(json)
                                                                        .accept(MediaType.APPLICATION_JSON)
                                                                        .contentType(MediaType.APPLICATION_JSON));
        addDishResult.andExpect(status().isOk());


        ResultActions queryOrderResult = mvc.perform(MockMvcRequestBuilders.get("/orders/" + orderId).accept(MediaType.APPLICATION_JSON));

        Double actualAmount = JsonPath.read(queryOrderResult.andReturn().getResponse().getContentAsString(), "$.amount");

        assertEquals(12.5, actualAmount);
    }

    @Test
    @DisplayName("Permet de récupérer le montant d'une commande avec deux plats")
    void testReturnsTotalAmountForTwoDishOrder() throws Exception {
        ResultActions createOrderResult = mvc.perform(MockMvcRequestBuilders.post("/orders").accept(MediaType.APPLICATION_JSON));
        String orderId = JsonPath.read(createOrderResult.andReturn().getResponse().getContentAsString(), "$");

        String expectedId = "vegan_nem";
        DishInOrder dish = new DishInOrder(expectedId);
        ObjectMapper objectMapper = new ObjectMapper();
        String json = objectMapper.writeValueAsString(dish);

        ResultActions addDishResult = mvc.perform(MockMvcRequestBuilders.post(
                MessageFormat.format("/orders/{0}/dishes", orderId))
                                                                        .content(json)
                                                                        .accept(MediaType.APPLICATION_JSON)
                                                                        .contentType(MediaType.APPLICATION_JSON));
        addDishResult.andExpect(status().isOk());

        expectedId = "rice";
        dish = new DishInOrder(expectedId);
        objectMapper = new ObjectMapper();
        json = objectMapper.writeValueAsString(dish);

        addDishResult = mvc.perform(MockMvcRequestBuilders.post(
                MessageFormat.format("/orders/{0}/dishes", orderId))
                                                                        .content(json)
                                                                        .accept(MediaType.APPLICATION_JSON)
                                                                        .contentType(MediaType.APPLICATION_JSON));
        addDishResult.andExpect(status().isOk());


        ResultActions queryOrderResult = mvc.perform(MockMvcRequestBuilders.get("/orders/" + orderId).accept(MediaType.APPLICATION_JSON));

        Double actualAmount = JsonPath.read(queryOrderResult.andReturn().getResponse().getContentAsString(), "$.amount");

        assertEquals(12.5 + 5.75, actualAmount);
    }
}