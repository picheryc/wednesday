package integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.JsonPath;
import domain.order.DishInOrder;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest(classes = {application.Application.class})
@AutoConfigureMockMvc
class DishIntegrationTest {
    @Autowired
    private MockMvc mvc;

    @Test
    @DisplayName("Permet de récupérer la liste des plats")
    void testCanQueryAllDishes() throws Exception {

        ResultActions getAllDishesResult = mvc.perform(MockMvcRequestBuilders.get("/dishes").accept(MediaType.APPLICATION_JSON));

        List<LinkedHashMap> dishes = JsonPath.read(getAllDishesResult.andReturn().getResponse().getContentAsString(), "$");
        List<String> dishesIds = new ArrayList(dishes.stream().map(order -> order.get("id")).collect(Collectors.toList()));
        assertTrue(dishesIds.contains("vegan_nem"));
        assertTrue(dishesIds.contains("rice"));
    }
}