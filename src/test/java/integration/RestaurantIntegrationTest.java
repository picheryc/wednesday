package integration;

import com.jayway.jsonpath.JsonPath;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertTrue;


@SpringBootTest(classes = {application.Application.class})
@AutoConfigureMockMvc
class RestaurantIntegrationTest {
    @Autowired
    private MockMvc mvc;

    @Test
    @DisplayName("Permet de récupérer la liste des tables")
    void testCanQueryAllDishes() throws Exception {

        ResultActions getAllTablesResult = mvc.perform(MockMvcRequestBuilders.get("/restaurant/tables").accept(MediaType.APPLICATION_JSON));

        List<LinkedHashMap> tables = JsonPath.read(getAllTablesResult.andReturn().getResponse().getContentAsString(), "$");
        List<String> tableIds = new ArrayList(tables.stream().map(table -> table.get("id")).collect(Collectors.toList()));
        assertTrue(tableIds.contains("3"));
        assertTrue(tableIds.contains("15"));
    }
}