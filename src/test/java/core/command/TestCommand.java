package core.command;

class TestCommand implements Command<TestResponse> {
    @Override
    public String getType() {
        return "TEST";
    }
}