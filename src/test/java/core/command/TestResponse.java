package core.command;

import core.command.response.CommandResponse;

class TestResponse implements CommandResponse {
    @Override
    public String getId() {
        return null;
    }

    @Override
    public boolean hasError() {
        return false;
    }
}