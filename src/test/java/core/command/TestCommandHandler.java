package core.command;

class TestCommandHandler implements CommandHandler<TestCommand, TestResponse> {
    private boolean called = false;

    @Override
    public TestResponse execute(TestCommand command) {
        this.called = true;
        return new TestResponse();
    }

    public boolean hasBeenCalled() {
        return this.called;
    }
}