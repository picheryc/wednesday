package core.command;

import core.command.response.CommandResponse;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;


class CommandBusTest {

    @Test
    @DisplayName("Permet de dispatcher une commande que l'on a souscrit")
    void testCanDispatchASubscribedCommand() {
        CommandBus sut = new CommandBus();
        TestCommandHandler handler = new TestCommandHandler();
        sut.subscribe("TEST", handler);

        sut.dispatch(new TestCommand());

        assertTrue(handler.hasBeenCalled());
    }

    @Test
    @DisplayName("ne permet pas de dispatcher une commande à laquelle on a pas souscrit")
    void testCannotDispatchAnUnsubscribedCommand() {
        CommandBus sut = new CommandBus();

        CommandResponse response = sut.dispatch(() -> "TEST");

        assertTrue(response.hasError());
    }

//    @Test
//    @DisplayName("Permet de dispatcher plusieurs commandes souscrites")
//    void testCanDispatchMultipleSubscribedCommands() {
//        CommandBus sut = new CommandBus();
//        List<Command> calledCommand = new ArrayList<>();
//        List<String> commandTypes = Arrays.asList("first", "second");
//        commandTypes
//                .forEach(commandType ->
//                        sut.subscribe(commandType, command -> {
//                            calledCommand.add(command);
//                            return new EmptyCommandResponse();
//                        }));
//
//        commandTypes
//                .forEach(commandType -> {
//                    CommandResponse response = sut.dispatch(() -> commandType);
//
//                    assertFalse(response.hasError());
//                    assertEquals(commandType, calledCommand.get(calledCommand.size() - 1).getType());
//                });
//
//    }
}