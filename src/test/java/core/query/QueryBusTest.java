package core.query;

import core.query.response.QueryResponse;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


class QueryBusTest {

    final String queryType = "TOTO";

    private QueryHandler buildQueryHandler(String response) {
        return query -> new QueryResponse() {
            @Override
            public Object getPayload() {
                return response;
            }

            @Override
            public boolean hasError() {
                return false;
            }
        };
    }

    @Test
    @DisplayName("Permet de dispatcher une query que l'on a souscrit")
    void testCanDispatchASubscribedQuery() {
        QueryBus sut = new QueryBus();
        QueryHandler handler = buildQueryHandler("ok");
        sut.subscribe(queryType, handler);

        Query totoQuery = () -> queryType;
        QueryResponse response = sut.dispatch(totoQuery);

        assertEquals("ok", response.getPayload());
        assertFalse(response.hasError());
    }


    @Test
    @DisplayName("Ne permet pas de dispatcher une query à laquelle on n'a pas souscrit")
    void testCannotDispatchAnUnSubscribedQuery() {
        QueryBus sut = new QueryBus();

        Query totoQuery = () -> queryType;
        QueryResponse response = sut.dispatch(totoQuery);

        assertNull(response.getPayload());
        assertTrue(response.hasError());
    }

    @Test
    @DisplayName("Permet de dispatcher differentes query souscrites")
    void testCanDispatchDifferentQueries() {
        QueryBus sut = new QueryBus();
        String first = "first";
        String second = "second";
        QueryHandler firstHandler = buildQueryHandler(first);
        QueryHandler secondHandler = buildQueryHandler(second);
        sut.subscribe(first, firstHandler);
        sut.subscribe(second, secondHandler);

        Query firstQuery = () -> first;
        Query secondQuery = () -> second;
        QueryResponse firstReponse = sut.dispatch(firstQuery);
        QueryResponse secondReponse = sut.dispatch(secondQuery);

        assertEquals(first, firstReponse.getPayload());
        assertEquals(second, secondReponse.getPayload());
        assertFalse(firstReponse.hasError());
        assertFalse(secondReponse.hasError());
    }
}
