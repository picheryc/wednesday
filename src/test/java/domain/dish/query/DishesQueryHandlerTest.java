package domain.dish.query;

import domain.dish.Dish;
import domain.dish.response.DishesResponse;
import infrastructure.repository.InMemoryRepository;
import infrastructure.repository.Repository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class DishesQueryHandlerTest {
    @Test
    @DisplayName("Renvoie la liste des plats existants")
    void testReturnExistingOrders() {
        Repository<Dish> dishRepository = new InMemoryRepository<>();
        Dish expectedDish = new Dish("toto", "toto", new BigDecimal(12));
        dishRepository.save(expectedDish.getId(), expectedDish);
        DishesQueryHandler sut = new DishesQueryHandler(dishRepository);

        DishesResponse response = sut.execute(new DishesQuery());

        final List<Dish> dishes = response.getPayload();
        assertNotNull(dishes);
        assertEquals(1, dishes.size());
        assertEquals(expectedDish.getId(), dishes.get(0).getId());
    }
}