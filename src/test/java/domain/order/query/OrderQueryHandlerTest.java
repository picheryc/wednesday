package domain.order.query;

import core.query.QueryBus;
import domain.dish.Dish;
import domain.dish.query.DishesQuery;
import domain.dish.query.DishesQueryHandler;
import domain.dish.repository.DishRepository;
import domain.order.DishInOrder;
import domain.order.Order;
import domain.order.response.OrderResponse;
import infrastructure.repository.InMemoryRepository;
import infrastructure.repository.Repository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;

class OrderQueryHandlerTest {
    @Test
    @DisplayName("Renvoie une commande existante")
    void testReturnExistingOrder() {
        Repository<Order> orderRepository = new InMemoryRepository<>();
        Order expectedOrder = new Order();
        String orderId = expectedOrder.getOrderId();
        orderRepository.save(orderId, expectedOrder);
        QueryBus queryBus = new QueryBus();
        OrderQueryHandler sut = new OrderQueryHandler(orderRepository, queryBus);

        OrderResponse response = sut.execute(new OrderQuery(orderId));

        Order order = response.getPayload();
        assertNotNull(order);
        assertEquals(expectedOrder, order);
    }

    @Test
    void testCallAllDishesQuery() {
        // Arrange
        Repository<Order> orderRepository = new InMemoryRepository<>();
        Order expectedOrder = new Order();
        String orderId = expectedOrder.getOrderId();
        expectedOrder.addDish(new DishInOrder("vegan_nem"));
        orderRepository.save(orderId, expectedOrder);
        QueryBus queryBus = new QueryBus();
        Repository<Dish> dishRepository = new DishRepository();
        dishRepository.save("vegan_nem", new Dish("vegan_nem", "Vegan nem", new BigDecimal("12.5")));
        queryBus.subscribe(DishesQuery.DISHES, new DishesQueryHandler(dishRepository));
        OrderQueryHandler sut = new OrderQueryHandler(orderRepository, queryBus);

        // Act
        OrderResponse response = sut.execute(new OrderQuery(orderId));

        // Assert
        Order order = response.getPayload();
        assertNotNull(order);
        assertEquals(new BigDecimal("12.5"), order.getAmount());
    }
}