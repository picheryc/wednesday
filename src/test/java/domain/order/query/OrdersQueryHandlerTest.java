package domain.order.query;

import domain.order.Order;
import domain.order.response.OrdersResponse;
import infrastructure.repository.InMemoryRepository;
import infrastructure.repository.Repository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class OrdersQueryHandlerTest {
    @Test
    @DisplayName("Renvoie la liste des commandes existantes")
    void testReturnExistingOrders() {
        Repository<Order> orderRepository = new InMemoryRepository<>();
        Order expectedOrder = new Order();
        String orderId = expectedOrder.getOrderId();
        orderRepository.save(orderId, expectedOrder);
        OrdersQueryHandler sut = new OrdersQueryHandler(orderRepository);

        OrdersResponse response = sut.execute(new OrdersQuery());

        final List<Order> orders = response.getPayload();
        assertNotNull(orders);
        assertEquals(1, orders.size());
        assertEquals(orderId, orders.get(0).getOrderId());
    }
}