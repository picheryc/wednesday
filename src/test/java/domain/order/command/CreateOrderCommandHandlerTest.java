package domain.order.command;

import domain.order.Order;
import domain.order.OrderFactory;
import domain.order.command.response.CreateOrderResponse;
import domain.order.response.OrderResponse;
import infrastructure.repository.InMemoryRepository;
import infrastructure.repository.Repository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class CreateOrderCommandHandlerTest {
    @Test
    @DisplayName("Renvoie la commande créée")
    void testReturnsCreatedOrder() {
        CreateOrderCommandHandler sut = new CreateOrderCommandHandler(new InMemoryRepository<>(), new OrderFactory());
        CreateOrderResponse response = sut.execute(new CreateOrderCommand());
        String orderId = response.getId();
        assertNotNull(orderId);
    }

    @Test
    @DisplayName("Sauvegarde la commande créée")
    void testSavesCreatedOrder() {
        Repository<Order> orderRepository = new InMemoryRepository<>();
        CreateOrderCommandHandler sut = new CreateOrderCommandHandler(orderRepository, new OrderFactory());
        CreateOrderResponse response = sut.execute(new CreateOrderCommand());
        String orderId = response.getId();
        Order savedOrder = orderRepository.read(orderId);
        assertNotNull(savedOrder);
    }
}
