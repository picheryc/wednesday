package domain.order.command;

import domain.order.DishInOrder;
import domain.order.Order;
import infrastructure.repository.InMemoryRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class AddDishToOrderCommandHandlerTest {
    @Test
    @DisplayName("Ajoute un plat à la commande")
    void testAddsDishToOrder() {
        final InMemoryRepository<Order> orderRepository = new InMemoryRepository<>();
        final String orderId = "toto";
        orderRepository.save(orderId, new Order());
        AddDishToOrderCommandHandler sut = new AddDishToOrderCommandHandler(orderRepository);

        final DishInOrder expected_dish = new DishInOrder("vegan_nem");
        sut.execute(new AddDishToOrderCommand(orderId, expected_dish));

        Order order = orderRepository.read(orderId);
        final List<DishInOrder> dishes = order.getDishes();
        assertEquals(1, dishes.size());
        assertEquals(expected_dish.id, dishes.get(0).id);
    }
}
