package domain.order.command;

import domain.order.Order;
import infrastructure.repository.InMemoryRepository;
import infrastructure.repository.Repository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNull;

class DeleteOrderCommandHandlerTest {
    @Test
    @DisplayName("Doit supprimer une commande")
    void testCanDeleteOrder() {
        Repository<Order> orderRepository = new InMemoryRepository<>();
        orderRepository.save("toto", new Order());
        DeleteOrderCommandHandler sut = new DeleteOrderCommandHandler(orderRepository);

        sut.execute(new DeleteOrderCommand("toto"));

        Order deletedOrder = orderRepository.read("toto");
        assertNull(deletedOrder);
    }
}