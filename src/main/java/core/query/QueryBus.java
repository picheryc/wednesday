package core.query;

import core.query.response.QueryFailure;
import core.query.response.QueryResponse;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class QueryBus {
    private final Map<String, QueryHandler> handlers;

    public QueryBus() {
        this.handlers = new HashMap<>();
    }

    public void subscribe(String type, QueryHandler handler) {
        handlers.put(type, handler);
    }

    public QueryResponse dispatch(Query query) {
        String queryType = query.getType();
        if (!handlers.containsKey(queryType)) return new QueryFailure();
        return handlers.get(queryType).execute(query);
    }
}
