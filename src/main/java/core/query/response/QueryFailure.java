package core.query.response;

public class QueryFailure implements QueryResponse {
    @Override
    public Object getPayload() {
        return null;
    }

    @Override
    public boolean hasError() {
        return true;
    }
}
