package core.query.response;

public interface QueryResponse<R> {
    R getPayload();

    boolean hasError();
}