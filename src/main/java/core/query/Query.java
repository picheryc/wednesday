package core.query;

import core.query.response.QueryResponse;

public interface Query<R extends QueryResponse> {
    String getType();
}
