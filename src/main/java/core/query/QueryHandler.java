package core.query;

import core.query.response.QueryResponse;

public interface QueryHandler<Q extends Query<R>, R extends QueryResponse> {
    R execute(Q query);
}
