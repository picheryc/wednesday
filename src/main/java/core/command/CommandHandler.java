package core.command;


import core.command.response.CommandResponse;

public interface CommandHandler<C extends Command<R>, R extends CommandResponse> {
    public R execute(C command);
}
