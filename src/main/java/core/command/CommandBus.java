package core.command;

import core.command.response.CommandFailure;
import core.command.response.CommandResponse;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class CommandBus {
    private Map<String, CommandHandler> commandHandlers;

    public CommandBus() {
        this.commandHandlers = new HashMap<>();
    }

    public void subscribe(String type, CommandHandler handler) {
        this.commandHandlers.put(type, handler);
    }

    public CommandResponse dispatch(Command command) {
        String commandType = command.getType();
        if (!commandHandlers.containsKey(commandType)) return new CommandFailure();
        return commandHandlers.get(commandType).execute(command);
    }
}
