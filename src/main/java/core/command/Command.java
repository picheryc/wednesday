package core.command;

import core.command.response.CommandResponse;

public interface Command<R extends CommandResponse> {
    String getType();
}
