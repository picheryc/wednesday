package core.command.response;

public class EmptyCommandResponse implements CommandResponse {
    @Override
    public String getId() {
        return null;
    }

    @Override
    public boolean hasError() {
        return false;
    }
}
