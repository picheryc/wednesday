package core.command.response;

public class CommandFailure implements CommandResponse {
    @Override
    public String getId() {
        return null;
    }

    @Override
    public boolean hasError() {
        return true;
    }
}
