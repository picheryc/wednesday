package core.command.response;

public interface CommandResponse {
    String getId();

    boolean hasError();
}
