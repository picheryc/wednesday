package infrastructure.repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class InMemoryRepository<T> implements Repository<T> {
    private HashMap<String, T> data;

    public InMemoryRepository() {
        this.data = new HashMap<String, T>();
    }

    @Override
    public void save(String key, T value) {
        this.data.put(key, value);
    }

    @Override
    public T read(String key) {
        return this.data.get(key);
    }

    @Override
    public List<T> list() {
        return new ArrayList<>(data.values());
    }

    @Override
    public void delete(String key) {
        data.remove(key);
    }
}
