package infrastructure.repository;

import java.util.List;

public interface Repository<T> {
    public void save(String key, T value);

    public T read(String key);

    public List<T> list();

    public void delete(String key);
}
