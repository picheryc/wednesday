package domain.dish.query;

import core.query.Query;
import domain.dish.response.DishesResponse;
import domain.order.response.OrdersResponse;

public class DishesQuery implements Query<DishesResponse> {
    public static final String DISHES = "DISHES";

    @Override
    public String getType() {
        return DISHES;
    }
}
