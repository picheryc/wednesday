package domain.dish.query;

import core.query.QueryHandler;
import domain.dish.Dish;
import domain.dish.response.DishesResponse;
import infrastructure.repository.Repository;

public class DishesQueryHandler implements QueryHandler<DishesQuery, DishesResponse> {
    private Repository<Dish> repository;

    public DishesQueryHandler(Repository<Dish> repository) {
        this.repository = repository;
    }

    @Override
    public DishesResponse execute(DishesQuery query) {
        return new DishesResponse(this.repository.list());
    }
}
