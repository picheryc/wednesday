package domain.dish.repository;

import domain.dish.Dish;
import infrastructure.repository.InMemoryRepository;

public class DishRepository extends InMemoryRepository<Dish> {
}
