package domain.dish;

import java.math.BigDecimal;

public class Dish {
    private final String id;
    private String name;
    private BigDecimal price;

    public Dish(String id, String name, BigDecimal price) {
        this.id = id;
        this.name = name;
        this.price = price;
    }

    public String getId() {
        return this.id;
    }

    public String getName() {
        return name;
    }

    public BigDecimal getPrice() {
        return price;
    }
}
