package domain.dish.response;

import core.query.response.QueryResponse;
import domain.dish.Dish;

import java.util.List;

public class DishesResponse implements QueryResponse<List<Dish>> {
    private List<Dish> dishes;

    public DishesResponse(List<Dish> dishes) {
        this.dishes = dishes;
    }

    @Override
    public List<Dish> getPayload() {
        return this.dishes;
    }

    @Override
    public boolean hasError() {
        return false;
    }
}
