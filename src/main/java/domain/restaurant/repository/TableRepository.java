package domain.restaurant.repository;

import domain.restaurant.Table;
import infrastructure.repository.InMemoryRepository;

public class TableRepository extends InMemoryRepository<Table> {
}
