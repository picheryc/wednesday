package domain.restaurant.response;

import core.query.response.QueryResponse;
import domain.restaurant.Table;

import java.util.List;

public class TablesResponse implements QueryResponse<List<Table>> {
    private List<Table> tables;

    public TablesResponse(List<Table> tables) {
        this.tables = tables;
    }

    @Override
    public List<Table> getPayload() {
        return this.tables;
    }

    @Override
    public boolean hasError() {
        return false;
    }
}
