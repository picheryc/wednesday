package domain.restaurant;

public class Table {

    private final String table;

    public Table(String table) {
        this.table = table;
    }

    public String getId() {
        return table;
    }
}
