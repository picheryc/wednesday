package domain.restaurant.query;

import core.query.QueryHandler;
import domain.restaurant.Table;
import domain.restaurant.response.TablesResponse;
import infrastructure.repository.Repository;

public class TablesQueryHandler implements QueryHandler<TablesQuery, TablesResponse> {
    private Repository<Table> repository;

    public TablesQueryHandler(Repository<Table> repository) {
        this.repository = repository;
    }

    @Override
    public TablesResponse execute(TablesQuery query) {
        return new TablesResponse(this.repository.list());
    }
}


