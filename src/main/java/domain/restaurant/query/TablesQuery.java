package domain.restaurant.query;

import core.query.Query;
import domain.order.response.OrdersResponse;
import domain.restaurant.response.TablesResponse;

public class TablesQuery implements Query<TablesResponse> {
    public static final String TABLES = "TABLES";

    @Override
    public String getType() {
        return TABLES;
    }
}
