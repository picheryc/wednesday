package domain.order;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class Order {
    private final String orderId;
    private final List<DishInOrder> dishes;
    private final String createdAt;
    private TableInOrder table;
    private BigDecimal amount;


    public Order() {
        this(UUID.randomUUID().toString());
    }

    public Order(String orderId) {
        this.orderId = orderId;
        this.createdAt = String.valueOf(System.currentTimeMillis());
        this.dishes = new ArrayList<>();
    }

    public String getOrderId() {
        return orderId;
    }

    public List<DishInOrder> getDishes() {
        return this.dishes;
    }

    public void addDish(DishInOrder dish) {
        this.dishes.add(dish);
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setTable(TableInOrder table) {
        this.table = table;
    }

    public TableInOrder getTable() {
        return table;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
