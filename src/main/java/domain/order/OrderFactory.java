package domain.order;

import org.springframework.stereotype.Service;

@Service
public class OrderFactory {
    private int count;

    public OrderFactory() {
        this.count = 0;
    }

    public Order createOrder() {
        this.count += 1;
        return new Order(String.valueOf(this.count));
    }
}
