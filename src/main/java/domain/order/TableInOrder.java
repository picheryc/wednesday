package domain.order;

public class TableInOrder {
    public final String id;

    public TableInOrder(String id) {
        this(id, "");
    }
    public TableInOrder(String id, String useless) {
        this.id = id;
    }
}
