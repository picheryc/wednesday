package domain.order.command;

import core.command.CommandHandler;
import domain.order.Order;
import domain.order.command.response.AddDishToOrderResponse;
import domain.order.command.response.AddTableToOrderResponse;
import infrastructure.repository.Repository;

public class AddTableToOrderCommandHandler implements CommandHandler<AddTableToOrderCommand, AddTableToOrderResponse> {

    private Repository<Order> orderRepository;

    public AddTableToOrderCommandHandler(Repository<Order> orderRepository) {
        this.orderRepository = orderRepository;
    }

    @Override
    public AddTableToOrderResponse execute(AddTableToOrderCommand command) {
        Order order = this.orderRepository.read(command.getOrderId());
        order.setTable(command.getTable());
        this.orderRepository.save(order.getOrderId(), order);
        return new AddTableToOrderResponse();
    }
}
