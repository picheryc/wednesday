package domain.order.command;

import core.command.CommandHandler;
import domain.order.Order;
import domain.order.OrderFactory;
import domain.order.command.response.CreateOrderResponse;
import infrastructure.repository.Repository;

public class CreateOrderCommandHandler implements CommandHandler<CreateOrderCommand, CreateOrderResponse> {

    private Repository<Order> orderRepository;
    private OrderFactory orderFactory;

    public CreateOrderCommandHandler(Repository<Order> orderRepository, OrderFactory orderFactory) {
        this.orderRepository = orderRepository;
        this.orderFactory = orderFactory;
    }

    @Override
    public CreateOrderResponse execute(CreateOrderCommand command) {
        Order order = orderFactory.createOrder();
        this.orderRepository.save(order.getOrderId(), order);
        return new CreateOrderResponse(order.getOrderId());
    }
}
