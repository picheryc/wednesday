package domain.order.command;

import core.command.Command;
import domain.order.command.response.DeleteOrderResponse;

public class DeleteOrderCommand implements Command<DeleteOrderResponse> {
    public static final String DELETE_ORDER = "DELETE_ORDER";
    private final String orderId;

    public DeleteOrderCommand(String orderId) {
        this.orderId = orderId;
    }

    @Override
    public String getType() {
        return DELETE_ORDER;
    }

    public String getOrderId() {
        return orderId;
    }
}