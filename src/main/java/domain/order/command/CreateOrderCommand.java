package domain.order.command;

import core.command.Command;
import domain.order.command.response.CreateOrderResponse;

public class CreateOrderCommand implements Command<CreateOrderResponse> {
    public static final String CREATE_ORDER = "CREATE_ORDER";

    @Override
    public String getType() {
        return CREATE_ORDER;
    }
}