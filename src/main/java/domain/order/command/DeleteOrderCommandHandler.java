package domain.order.command;

import core.command.CommandHandler;
import domain.order.Order;
import domain.order.command.response.DeleteOrderResponse;
import infrastructure.repository.Repository;

public class DeleteOrderCommandHandler implements CommandHandler<DeleteOrderCommand, DeleteOrderResponse> {

    private Repository<Order> orderRepository;

    public DeleteOrderCommandHandler(Repository<Order> orderRepository) {
        this.orderRepository = orderRepository;
    }

    @Override
    public DeleteOrderResponse execute(DeleteOrderCommand command) {
        this.orderRepository.delete(command.getOrderId());
        return new DeleteOrderResponse();
    }
}
