package domain.order.command.response;

import core.command.response.CommandResponse;

public class CreateOrderResponse implements CommandResponse {

    private String orderId;

    public CreateOrderResponse(String orderId) {
        this.orderId = orderId;
    }

    @Override
    public String getId() {
        return orderId;
    }

    @Override
    public boolean hasError() {
        return false;
    }
}
