package domain.order.command;

import core.command.Command;
import domain.order.TableInOrder;
import domain.order.command.response.AddTableToOrderResponse;

public class AddTableToOrderCommand implements Command<AddTableToOrderResponse> {
    public static final String ADD_TABLE_TO_ORDER = "ADD_TABLE_TO_ORDER";
    private String orderId;
    private TableInOrder table;

    public AddTableToOrderCommand(String orderId, TableInOrder table) {
        this.orderId = orderId;
        this.table = table;
    }

    @Override
    public String getType() {
        return ADD_TABLE_TO_ORDER;
    }

    public String getOrderId() {
        return orderId;
    }

    public TableInOrder getTable() {
        return table;
    }
}
