package domain.order.command;

import core.command.Command;
import domain.order.DishInOrder;
import domain.order.command.response.AddDishToOrderResponse;

public class AddDishToOrderCommand implements Command<AddDishToOrderResponse> {
    public static final String ADD_DISH_TO_ORDER = "ADD_DISH_TO_ORDER";
    private String orderId;
    private DishInOrder dish;

    public AddDishToOrderCommand(String orderId, DishInOrder dish) {
        this.orderId = orderId;
        this.dish = dish;
    }

    @Override
    public String getType() {
        return ADD_DISH_TO_ORDER;
    }

    public String getOrderId() {
        return orderId;
    }

    public DishInOrder getDish() {
        return dish;
    }
}
