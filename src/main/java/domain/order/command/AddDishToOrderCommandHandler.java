package domain.order.command;

import core.command.CommandHandler;
import domain.order.Order;
import domain.order.command.response.AddDishToOrderResponse;
import infrastructure.repository.Repository;

public class AddDishToOrderCommandHandler implements CommandHandler<AddDishToOrderCommand, AddDishToOrderResponse> {

    private Repository<Order> orderRepository;

    public AddDishToOrderCommandHandler(Repository<Order> orderRepository) {
        this.orderRepository = orderRepository;
    }

    @Override
    public AddDishToOrderResponse execute(AddDishToOrderCommand command) {
        Order order = this.orderRepository.read(command.getOrderId());
        order.addDish(command.getDish());
        this.orderRepository.save(order.getOrderId(), order);
        return new AddDishToOrderResponse();
    }
}
