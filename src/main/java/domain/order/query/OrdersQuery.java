package domain.order.query;

import core.query.Query;
import domain.order.response.OrdersResponse;

public class OrdersQuery implements Query<OrdersResponse> {
    public static final String ORDERS = "ORDERS";

    @Override
    public String getType() {
        return ORDERS;
    }
}
