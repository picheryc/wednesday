package domain.order.query;

import core.query.Query;
import domain.order.response.OrderResponse;

public class OrderQuery implements Query<OrderResponse> {
    private final String orderId;
    public static final String ORDER = "ORDER";

    public OrderQuery(String orderId) {
        this.orderId = orderId;
    }

    @Override
    public String getType() {
        return ORDER;
    }

    public String getOrderId() {
        return orderId;
    }
}
