package domain.order.query;

import core.query.QueryHandler;
import domain.order.Order;
import domain.order.response.OrdersResponse;
import infrastructure.repository.Repository;

public class OrdersQueryHandler implements QueryHandler<OrdersQuery, OrdersResponse> {
    private Repository<Order> repository;

    public OrdersQueryHandler(Repository<Order> repository) {
        this.repository = repository;
    }

    @Override
    public OrdersResponse execute(OrdersQuery query) {
        return new OrdersResponse(this.repository.list());
    }
}


