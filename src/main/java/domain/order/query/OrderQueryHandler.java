package domain.order.query;

import core.query.QueryBus;
import core.query.QueryHandler;
import core.query.response.QueryResponse;
import domain.dish.Dish;
import domain.dish.query.DishesQuery;
import domain.dish.response.DishesResponse;
import domain.order.DishInOrder;
import domain.order.Order;
import domain.order.response.OrderResponse;
import infrastructure.repository.Repository;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

public class OrderQueryHandler implements QueryHandler<OrderQuery, OrderResponse> {
    private Repository<Order> repository;
    private QueryBus queryBus;

    public OrderQueryHandler(Repository<Order> repository, QueryBus queryBus) {
        this.repository = repository;
        this.queryBus = queryBus;
    }

    @Override
    public OrderResponse execute(OrderQuery query) {
        List<Dish> dishes = (List<Dish>) queryBus.dispatch(new DishesQuery()).getPayload();
        Order order = this.repository.read(query.getOrderId());
        BigDecimal amount = BigDecimal.ZERO;
        for (DishInOrder dish : order.getDishes()) {
            BigDecimal dishAmount = dishes.stream()
                                          .filter(d -> d.getId().equals(dish.id))
                                          .map(Dish::getPrice)
                                          .findFirst()
                                          .orElse(BigDecimal.ZERO);
            amount = amount.add(dishAmount);
        }
        order.setAmount(amount);
        return new OrderResponse(order);
    }
}


