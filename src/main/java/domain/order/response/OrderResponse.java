package domain.order.response;

import core.query.response.QueryResponse;
import domain.order.Order;

public class OrderResponse implements QueryResponse<Order> {
    private Order order;

    public OrderResponse(Order order) {
        this.order = order;
    }

    @Override
    public Order getPayload() {
        return this.order;
    }

    @Override
    public boolean hasError() {
        return false;
    }
}
