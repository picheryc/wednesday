package domain.order.response;

import core.query.response.QueryResponse;
import domain.order.Order;

import java.util.List;

public class OrdersResponse implements QueryResponse<List<Order>> {
    private List<Order> orders;

    public OrdersResponse(List<Order> orders) {
        this.orders = orders;
    }

    @Override
    public List<Order> getPayload() {
        return this.orders;
    }

    @Override
    public boolean hasError() {
        return false;
    }
}
