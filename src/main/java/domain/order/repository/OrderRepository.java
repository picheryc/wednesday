package domain.order.repository;

import domain.order.Order;
import infrastructure.repository.InMemoryRepository;

public class OrderRepository extends InMemoryRepository<Order> {
}
