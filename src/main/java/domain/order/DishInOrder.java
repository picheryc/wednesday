package domain.order;

public class DishInOrder {
    public final String id;
    private String remarks;

    public DishInOrder(String id, String remarks) {
        this.id = id;
        this.remarks = remarks;
    }

    public DishInOrder(String id) {
        this(id, "");
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }
}
