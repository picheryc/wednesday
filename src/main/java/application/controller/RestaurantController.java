package application.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import core.query.QueryBus;
import domain.restaurant.Table;
import domain.restaurant.query.TablesQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin
@RestController
public class RestaurantController {
    private final QueryBus queryBus;

    public RestaurantController(@Autowired QueryBus queryBus) {
        this.queryBus = queryBus;
    }

    @GetMapping(path = "/restaurant/tables", produces = MediaType.APPLICATION_JSON_VALUE)
    public String getAllDishes() throws JsonProcessingException {
        List<Table> tables = (List<Table>) queryBus.dispatch(new TablesQuery()).getPayload();
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(tables);
    }
}