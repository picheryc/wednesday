package application.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import core.query.QueryBus;
import domain.dish.Dish;
import domain.dish.query.DishesQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin
@RestController
public class DishController {
    private final QueryBus queryBus;

    public DishController(@Autowired QueryBus queryBus) {
        this.queryBus = queryBus;
    }

    @GetMapping(path = "/dishes", produces = MediaType.APPLICATION_JSON_VALUE)
    public String getAllDishes() throws JsonProcessingException {
        List<Dish> dishes = (List<Dish>) queryBus.dispatch(new DishesQuery()).getPayload();
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(dishes);
    }
}