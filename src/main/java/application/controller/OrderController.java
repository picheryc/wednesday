package application.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import core.command.CommandBus;
import core.query.QueryBus;
import domain.order.DishInOrder;
import domain.order.Order;
import domain.order.TableInOrder;
import domain.order.command.AddDishToOrderCommand;
import domain.order.command.AddTableToOrderCommand;
import domain.order.command.CreateOrderCommand;
import domain.order.command.DeleteOrderCommand;
import domain.order.query.OrderQuery;
import domain.order.query.OrdersQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
public class OrderController {
    private final CommandBus commandBus;
    private final QueryBus queryBus;

    public OrderController(@Autowired CommandBus commandBus, @Autowired QueryBus queryBus) {
        this.commandBus = commandBus;
        this.queryBus = queryBus;
    }

    @PostMapping(path = "/orders",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public String createOrder() throws JsonProcessingException {
        String orderId = commandBus.dispatch(new CreateOrderCommand()).getId();
        ObjectMapper objectMapper = new ObjectMapper();
        String json = objectMapper.writeValueAsString(orderId);
        return json;
    }

    @PostMapping(path = "/orders/{orderId}/dishes",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public void addDishToOrder(@PathVariable String orderId, @RequestBody DishInOrder dish) {
        commandBus.dispatch(new AddDishToOrderCommand(orderId, dish));
    }

    @PostMapping(path = "/orders/{orderId}/table",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public void addTableToOrder(@PathVariable String orderId, @RequestBody TableInOrder table) {
        commandBus.dispatch(new AddTableToOrderCommand(orderId, table));
    }


    @GetMapping(path = "/orders/{orderId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public String getOrder(@PathVariable String orderId) throws JsonProcessingException {
        Order order = (Order) queryBus.dispatch(new OrderQuery(orderId)).getPayload();
        ObjectMapper objectMapper = new ObjectMapper();
        String json = objectMapper.writeValueAsString(order);
        return json;
    }

    @GetMapping(path = "/orders", produces = MediaType.APPLICATION_JSON_VALUE)
    public String getOrder() throws JsonProcessingException {
        List<Order> orders = (List<Order>) queryBus.dispatch(new OrdersQuery()).getPayload();
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(orders);
    }

    @DeleteMapping(path = "/orders/{orderId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public void deleteOrder(@PathVariable String orderId) throws JsonProcessingException {
        commandBus.dispatch(new DeleteOrderCommand(orderId));
    }
}