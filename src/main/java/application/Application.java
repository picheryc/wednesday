package application;

import core.command.CommandBus;
import core.query.QueryBus;
import domain.dish.Dish;
import domain.dish.query.DishesQuery;
import domain.dish.query.DishesQueryHandler;
import domain.dish.repository.DishRepository;
import domain.order.OrderFactory;
import domain.order.command.*;
import domain.order.query.OrderQuery;
import domain.order.query.OrderQueryHandler;
import domain.order.query.OrdersQuery;
import domain.order.query.OrdersQueryHandler;
import domain.order.repository.OrderRepository;
import domain.restaurant.Table;
import domain.restaurant.query.TablesQuery;
import domain.restaurant.query.TablesQueryHandler;
import domain.restaurant.repository.TableRepository;
import infrastructure.repository.Repository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

@SpringBootApplication(scanBasePackages = {"application.controller", "core"})
public class Application {

    private final CommandBus commandBus;
    private final QueryBus queryBus;

    public Application(@Autowired CommandBus commandBus, @Autowired QueryBus queryBus) {
        this.commandBus = commandBus;
        this.queryBus = queryBus;
        Map<String, Repository> repositories = createRepositories();
        registerCommands(repositories);
        registerQueries(repositories);
    }

    private Map<String, Repository> createRepositories() {
        Map<String, Repository> repositories = new HashMap<>();
        Repository orderRepository = new OrderRepository();
        DishRepository dishRepository = new DishRepository();
        this.initDishRepository(dishRepository);
        TableRepository tableRepository = new TableRepository();
        this.initTableRepository(tableRepository);
        repositories.put("order", orderRepository);
        repositories.put("dish", dishRepository);
        repositories.put("table", tableRepository);
        return repositories;
    }

    private void initDishRepository(DishRepository dishRepository) {
        dishRepository.save("vegan_nem", new Dish("vegan_nem", "Nem végan", new BigDecimal(12.50)));
        dishRepository.save("pho", new Dish("pho", "Pho au thon rouge", new BigDecimal(9.25)));
        dishRepository.save("loklak_beef", new Dish("loklak_beef", "Boeuf loklak", new BigDecimal(14.15)));
        dishRepository.save("rice", new Dish("rice", "Riz blanc", new BigDecimal(5.75)));
    }

    private void initTableRepository(TableRepository tableRepository) {
        tableRepository.save("15", new Table("15"));
        tableRepository.save("25", new Table("25"));
        tableRepository.save("3", new Table("3"));
        tableRepository.save("17", new Table("17"));
    }

    private void registerCommands(Map<String, Repository> repositories) {
        OrderFactory orderFactory = new OrderFactory();
        final OrderRepository orderRepository = (OrderRepository) repositories.get("order");
        commandBus.subscribe(CreateOrderCommand.CREATE_ORDER, new CreateOrderCommandHandler(orderRepository, orderFactory));
        commandBus.subscribe(AddDishToOrderCommand.ADD_DISH_TO_ORDER, new AddDishToOrderCommandHandler(orderRepository));
        commandBus.subscribe(AddTableToOrderCommand.ADD_TABLE_TO_ORDER, new AddTableToOrderCommandHandler(orderRepository));
        commandBus.subscribe(DeleteOrderCommand.DELETE_ORDER, new DeleteOrderCommandHandler(orderRepository));
    }

    private void registerQueries(Map<String, Repository> repositories) {
        queryBus.subscribe(OrderQuery.ORDER, new OrderQueryHandler((OrderRepository) repositories.get("order"), queryBus));
        queryBus.subscribe(OrdersQuery.ORDERS, new OrdersQueryHandler((OrderRepository) repositories.get("order")));
        queryBus.subscribe(DishesQuery.DISHES, new DishesQueryHandler((DishRepository) repositories.get("dish")));
        queryBus.subscribe(TablesQuery.TABLES, new TablesQueryHandler((TableRepository) repositories.get("table")));
    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}